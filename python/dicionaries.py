dict = {'numero':1.2,"string":"nome",'lista':[1,2,[1,2,3]]}
print(type(dict))
print(dict['string'][::-1].upper())
print(dict['lista'][2][0])
dict["string"] = 'vida'
dict['keys'] = {'key1':'oi'}
print(dict.keys())
print(list(dict.keys())[1])
print(dict.values())
print(dict.items())
